import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import {ThemeProvider, createTheme} from "@mui/material"

const theme = createTheme({
  palette : {
    primary: {
        main: '#566885'
  }},
  secondary: {
    main: '#00ff00'
}
});

ReactDOM.render(
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>,
  document.getElementById('root')
);


